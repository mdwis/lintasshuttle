<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
  
/** 
 * Layouts Class. PHP5 only. 
 * 
 */
class Layouts { 
    
  // Will hold a CodeIgniter instance 
  private $CI; 
    
  public function __construct()  
  { 
    $this->CI =& get_instance(); 
  } 
    
  public function display($data = null) 
  { 
    $data['_header'] = $this->CI->load->view('_header',$data,true);
    $this->CI->load->view('Layouts',$data);
  } 
}