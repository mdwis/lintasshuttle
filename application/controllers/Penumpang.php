<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penumpang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_booking');
		$this->load->model('m_penumpang');
	}

	public function index()
	{
		// if($this->session->userdata('logged_in')) { //Cek session login atau belum
  //            $role = $this->session->role;
  //           if ($role == 'user') { //Cek session hak akses admin
  //               redirect (base_url('Penumpang'));
  //           }else
  //           {
  //               redirect(base_url('login/dashboardLoginUser'));
  //           }
  //       }else {
  //           $data = array('alert' => false);
  //           $this->load->view('login/landing_page',$data);
  //       }
		$this->load->view('User/dashboard');
	}
	public function lihatPesanan($id="")
	{
		$id 			= $this->session->idPenumpang;
		$idTransaksi 	= $this->session->idTransaksi;
		$data = $this->m_penumpang->getAllPenumpangTransaction($id);
		$data ['transaksi']	= $data;
		$this->index();
		$this->load->view('User/lihatPesanan',$data);
	}
	public function rubahJadwal()
	{
		$where=array(
			'idTransaksi'=>$this->session->userdata('idTransaksi')
		);
		$this->index();
		$this->load->view('User/rubahJadwal');
	}
	// public function batalkanPesanan()
	// {
		
	// }
}
