<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	public function header()
	{
		$this->load->view('_header');
	}
	public function index()
	{
		$this->header();
		$this->load->view('Content/dashboard');
	}
}
