<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {
  private $data;  

  public function __construct()
    {
        parent::__construct();
        $this->load->model('m_booking','dep_model', TRUE);
        $this->load->model('m_penumpang');
        $this->load->model('m_tiket');
        $this->load->library('Tamasms');    
    }
  public function send(){
    // $to = $this->input->post("to");
    // $message = $this->input->post("message", true);
    $respon = "<div class='alert alert-success' role='alert'>This is a success alert—check it out!</div>";
    $to= "081214827906";
    $message = "Terimakasih telah melakukan booking";
    $this->tamasms->setIP('192.168.1.6:8080');
    echo $this->tamasms->sendSMS($to, $message);

  }
  public function header()
  {
    $this->load->view('_header');
  }

  public function index()
  {
    $this->header();
    $getAllTiket= $this->dep_model->getDataTiket();
    $data['tiket'] = $getAllTiket;
    $this->load->view('Pemesanan/dataPenumpang',$data);
    //$this->load->view('Welcome_message');
  }
  public function cari($keberangkatan="",$tujuan="",$tanggalKeberangkatan="")
  {
      $tanggalKeberangkatan = $this->input->post('tanggalKeberangkatan');
      $jumlah               = $this->input->post('jumlah');
      $keberangkatan        = $this->input->post('keberangkatan');
      $tujuan               = $this->input->post('tujuan');
      $data =array (
        'tanggalKeberangkatan' =>$tanggalKeberangkatan,
        'jumlah'               =>$jumlah);
      $this->session->set_userdata($data);
      $datahasil['hasil'] = $this->m_tiket->cariTiket($keberangkatan,$tujuan);
      $q = $this->session->set_userdata($datahasil);
      $this->header();
      $this->load->view('Pemesanan/dataPenumpang',$datahasil);
  }
  public function insertTransaksi($hasil="")
  {
    
    $idtiket                      = $this->uri->segment(3);
    $t_keberangkatan              = $this->uri->segment(4);
    $t_tujuan                     = $this->uri->segment(5);
    $t_harga                      = $this->uri->segment(6);
    $t_jam                        = $this->uri->segment(7);
    $tanggalKeberangkatan = $this->session->userdata('tanggalKeberangkatan');
    $jumlah               = $this->session->userdata('jumlah');
    $data = array(
      'tanggalKeberangkatan'  =>$tanggalKeberangkatan,
      'jumlah'                =>$jumlah,
      't_keberangkatan'       =>$t_keberangkatan,
      't_tujuan'              =>$t_tujuan,
      't_harga'               =>$t_harga,
      't_jam'                 =>$t_jam);
    $respon = $this->dep_model->insert($data);
    
    redirect('booking/pilihKursi/'.$this->db->insert_id());
  }
  

  public function ajaxGetTujuan()
  {
    $dataTiket     = $this->dep_model->getDataTiket();
    $keberangkatan        = $this->input->post('keberangkatan');
    //$provinces = $this->dep_model->get_province_query($country_id);
    if($keberangkatan == '')
    {
      $pro_select_box = '';
      $pro_select_box .= '<option value="">Pilih Tujuan</option>';
      foreach ($dataTiket as $tiketData) {
        $pro_select_box .='<option value="'.$tiketData->Tujuan.'">'.$tiketData->Tujuan.'</option>';
      }
      echo json_encode($pro_select_box);
    }
  }
  public function ajaxGetJadwal()
  {
    $keberangkatan = $this->input->post('Keberangkatan');
    if (!empty($keberangkatan)) {
      $output = '';
      $dataTiket = $this->dep_model->getDataTiket();
        foreach ($dataTiket as $row) {
            $output = "<tr>";
            $output .= "<td>".$row->Tujuan;
            $output .= "<td>".$row->Harga;
            $output .= "<td>".$row->Jam;
            $output .= "<td>".$row->Stok;
            $output .= "</td>";
            $output .= "</tr>";

        }
       echo $output;
    }
  }
  public function pilihJam($id="")
  {
    $tiket = $this->dep_model->getDataTiket($id);
    $transaksi = $this->dep_model->getDataId($id);
    $data ['tiket'] = $tiket;
    $data ['transaksi'] = $transaksi;
    $getTransaksi['tiket'] = $this->dep_model->getAllTransaksi();
    $this->header();
    $this->load->view('Pemesanan/pilihJam',$data);
  }
  public function updateJam($id="",$data="")
  {
    $data = array ('jam' => $this->input->post('jam',true));
     $respon = $this->dep_model->updateJam($id,$data);
    if($respon){
      echo "Success";
      redirect(base_url('Booking/pilihKursi/'.$id));
      //redirect('Booking/pilihKursi/'.$this->db->insert_id());
      //redirect(base_url('booking/pilihKursi'));
    }else{
      echo "gagal";
      redirect(base_url('Booking/pilihKursi/'.$id));
    }
    //redirect('');
  }
  public function pilihKursi($id="" )
  {
    $iduri = $this->uri->segment(3);
    $this->load->model('m_transksi');
    $tiket = $this->dep_model->getDataTiket($id);
    $transaksi = $this->dep_model->getDataId($id);
    $data ['tiket'] = $tiket;
    $data ['transaksi'] = $transaksi;
    $getTransaksi['tiket'] = $this->dep_model->getAllTransaksi();
    $this->header();
    $this->load->view('Pemesanan/pilihKursi',$data);
  }
  public function updateKursi($id="",$data="")
  {
    $data = array ('tempatDuduk' => $this->input->post('tempatDuduk',true));
     $respon = $this->dep_model->updateKursi($id,$data);
    if($respon){
      echo "Success";
      redirect(base_url('Booking/dataPenumpang/'.$id));
    }else{
      echo "gagal";
      redirect(base_url('Booking/dataPenumpang/'.$id));
    }
  }
  public function dataPenumpang($id="")
  {
    $transaksi = $this->dep_model->getDataId($id);
    $data ['transaksi'] = $transaksi;
    $this->header();
    $this->load->view('Pemesanan/isiDataPenumpang',$data);
  }
  public function insertDataPemesan($data="",$id="")
  {
    $this->load->model('m_penumpang');
    $noHp = "0".$this->input->post('noHp');
      $data = array(
            'idTransaksi'   =>$this->input->post('id'),
            'namaPenumpang' =>$this->input->post('nama'),
            'jenisKelamin' =>$this->input->post('jenisKelamin'),
            'email'       =>$this->input->post('email'),
            'noHp'        =>$noHp,
            'username'     =>$this->input->post('username'),
            'password'     =>$this->input->post('password'),
            'alamat'       =>$this->input->post('alamat'),
            'role'    => 'user',
            'status'  => '1'
          );
      $respon = $this->m_penumpang->insertPenumpang($data);
      if ($respon) {
        echo "Success";
        redirect('Booking/detailTransaction/'.$this->db->insert_id());
        //redirect(base_url('Booking/detailTransaction/'.$id));
      }else
      {
        echo "Gagal";
        redirect('Booking/detailTransaction/'.$this->db->insert_id());
        //redirect(base_url('Booking/detailTransaction/'.$id));
      }
  }
  public function detailTransaction($id="")
  {
   $id = $this->uri->segment(3);
    $join = $this->m_penumpang->getAllPenumpangTransaction($id);
    $allData ['data'] = $join;
    //$allData['penumpang'] = $penumpang;
     $this->header();
    $this->load->view('Pemesanan/detailTransaction',$allData);
  }
  public function depan()
  {
    $this->header();
    $this->load->view('_depan');
        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //   CURLOPT_URL => "https://panel.apiwha.com/send_message.php?apikey=NCT2ANP90TFHHIVFLHH5&number=+6281284708100&text=Test API Whatapp",
        //   CURLOPT_RETURNTRANSFER => true,
        //   CURLOPT_ENCODING => "",
        //   CURLOPT_MAXREDIRS => 10,
        //   CURLOPT_TIMEOUT => 30,
        //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //   CURLOPT_CUSTOMREQUEST => "GET",
        // ));

        // $response = curl_exec($curl);
        // $err = curl_error($curl);

        // curl_close($curl);

        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   echo $response;
        // }
  }
     public function cetakTiket($idPengguna = "")
    {   
        $idPengguna =$this->uri->segment(3);
        var_dump($idPengguna);
        print_r($idPengguna);
        die();
        $pengguna = $this->m_penumpang->penumpangId($idPengguna);
        die();
        $data = [];
                $html = "<table style='border-collapse: collapse;'>
<tbody>
<tr style='height: 27.9pt;'>
<td style='width: 467.5pt; border: none; border-bottom: solid #7F7F7F 1.0pt; background: #D5DCE4; padding: 0cm 5.4pt 0cm 5.4pt; height: 27.9pt;' colspan='6' width='623'>
<p style='margin-bottom: .0001pt; text-align: left; line-height: normal;'><em><span style='font-size: 13.0pt; font-family: 'Calibri Light',sans-serif;'>Account and Transaction Report</span></em></p>
</td>
</tr>
<tr style='height: 28.3pt;'>
<td style='width: 467.5pt; border: none; border-right: solid #7F7F7F 1.0pt; background: white; padding: 0cm 5.4pt 0cm 5.4pt; height: 28.3pt;' colspan='6' width='623'>
<p style='margin-bottom: .0001pt; text-align: left; line-height: normal;'><em><span style='font-size: 13.0pt; font-family: 'Calibri Light',sans-serif;'>Nama : ".$pengguna['namaPenumpang']." </span></em></p>
</td>
</tr>
<tr style='height: 41.45pt;'>
<td style='width: 467.5pt; border: none; border-right: solid #7F7F7F 1.0pt; background: white; padding: 0cm 5.4pt 0cm 5.4pt; height: 41.45pt;' colspan='6' width='623'>
<p style='margin-bottom: .0001pt; text-align: left; line-height: normal;'><em><span style='font-size: 13.0pt; font-family: 'Calibri Light',sans-serif;'>Transaction Number # </span></em></p>
</td>
</tr>
<tr style='height: 20.95pt;'>
<td style='width: 94.7pt; border: none; border-right: solid #7F7F7F 1.0pt; background: white; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='126'>
<p style='margin-bottom: .0001pt; text-align: right; line-height: normal;'><em><span style='font-size: 13.0pt; font-family: 'Calibri Light',sans-serif;'>Tanggal Keberangkatan</span></em></p>
</td>
<td style='width: 86.7pt; background: #F2F2F2; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='116'>
<p style='margin-bottom: .0001pt; line-height: normal;'>Jumlah Penumpang</p>
</td>
<td style='width: 94.75pt; background: #F2F2F2; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='126'>
<p style='margin-bottom: .0001pt; line-height: normal;'>Keberangkatan</p>
</td>
<td style='width: 73.55pt; background: #F2F2F2; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='98'>
<p style='margin-bottom: .0001pt; line-height: normal;'>Tujuan</p>
</td>
<td style='width: 61.65pt; background: #F2F2F2; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='82'>
<p style='margin-bottom: .0001pt; line-height: normal;'>Jam</p>
</td>
<td style='width: 56.15pt; background: #F2F2F2; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='75'>
<p style='margin-bottom: .0001pt; line-height: normal;'>Nomor Kursi</p>
</td>
</tr>
<tr style='height: 20.95pt;'>
<td style='width: 94.7pt; border: none; border-right: solid #7F7F7F 1.0pt; background: white; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='126'>
<p style='margin-bottom: .0001pt; text-align: right; line-height: normal;'><em><span style='font-size: 13.0pt; font-family: 'Calibri Light',sans-serif;'>15-06-1998</span></em></p>
</td>
<td style='width: 86.7pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='116'>
<p style='margin-bottom: .0001pt; line-height: normal;'>1</p>
</td>
<td style='width: 94.75pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='126'>
<p style='margin-bottom: .0001pt; line-height: normal;'>Bandung</p>
</td>
<td style='width: 73.55pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='98'>
<p style='margin-bottom: .0001pt; line-height: normal;'>Jakarta</p>
</td>
<td style='width: 61.65pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='82'>
<p style='margin-bottom: .0001pt; line-height: normal;'>10.00</p>
</td>
<td style='width: 56.15pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 20.95pt;' width='75'>
<p style='margin-bottom: .0001pt; line-height: normal;'>10</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>";
        //this the the PDF filename that user will get to download
        $pdfFilePath = "Tiket.pdf";
 
        //load mPDF library
        $this->load->library('M_pdf');
 
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 

    }
    public function paymentMethod()
    {
      $this->header();
      $this->load->view('Pemesanan/paymentMehtod');
    }
}
