<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	private $data;

	public function __construct()
    {
        parent::__construct();
        //$this->load->model('participantmodel');
		$this->load->model('m_login');
    }
    public function index()
    {
    	if($this->session->userdata('logged_in')) { //Cek session login atau belum
             $role = $this->session->role;
            if ($role == 'admin') { //Cek session hak akses admin
                redirect (base_url('petugas/sideBar'));
            }else
            {
                redirect(base_url('login'));
            }
            die();
            redirect(base_url("dashboard"));
        }else {
            $data = array('alert' => false);
            $this->load->view('login/landing_page',$data);
        }
    }
    public function login(){
        $postData = $this->input->post();
        $validate = $this->m_login->validate_login($postData);
        $validate2 = $this->m_login->validate_loginUser($postData);
        // print_r($validate);
        // die();
        if ($validate[0]){
            $newdata = array(                               //Getting data from tabel
                'username'     => $validate[1]->username,
                'namaPetugas' => $validate[1]->namaPetugas,
                'role' => $validate[1]->role,
                'idPetugas' => $validate[1]->idPetugas,
                'logged_in' => TRUE,
              
            );
            $this->session->set_userdata($newdata);
            redirect(base_url('Petugas'));
        }elseif ($validate2[0]){
        $newdata = array(                               //Getting data from tabel
                'username'     => $validate2[1]->username,
                'namaPenumpang' => $validate2[1]->namaPenumpang,
                'role' => $validate2[1]->role,
                'idPenumpang' => $validate2[1]->idPenumpang,
                'logged_in' => TRUE,
              
            );
            $this->session->set_userdata($newdata);
            redirect(base_url('Petugas'));
        }
        else{
            $data = array('alert' => true);
            $this->load->view('login/landing_page',$data);
        }
     
    }
    public function loginUser()
    {
         $m =$this->input->post();
        // print_r($m);
        // die();
        $data  = $this->m_login->cek_pengguna();
        if($data->num_rows() > 0 ){
            $res = $data->row_array();
            $this->session->set_flashdata('message',"Selamat Anda Berhasil Login.");
            $this->session->nama = $res['idTransaksi'];
            foreach ($data->result()as $res) {
                $username = $res->username;
                $idPenumpang = $res->idPenumpang;
                $idTransaksi = $res->idTransaksi;
                $role        = $res->role;
            }
            $data = array('idTransaksi'=>$idTransaksi,
                          'idPenumpang'=>$idPenumpang,
                            'role'     =>$role);
            $this->session->set_userdata($data);
            redirect('penumpang');
        }else
        {

            $data = array('alert' => true);
            $this->load->view('login/login_user',$data);
        }
    }
    public function dashboardLoginUser()
    {
        $this->load->view('Login/login_user');
    }
    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
