<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_pembayaran');
        $this->load->library('form_validation');
    }
    public function sideBar()
    {
        $this->load->view('Admin/_sideBar');
    }
    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pembayaran/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pembayaran/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pembayaran/index.html';
            $config['first_url'] = base_url() . 'pembayaran/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->M_pembayaran->total_rows($q);
        $pembayaran = $this->M_pembayaran->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pembayaran_data' => $pembayaran,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->sideBar();
        $this->load->view('pembayaran/pembayaran_list', $data);
    }

    public function read($id) 
    {
        $row = $this->M_pembayaran->get_by_id($id);
        if ($row) {
            $data = array(
		'idPembayaran' => $row->idPembayaran,
		'namaPetugas' => $row->namaPetugas,
		'namaBank' => $row->namaBank,
		'nomorRekening' => $row->nomorRekening,
		'namaPemilik' => $row->namaPemilik,
		'Deskripsi' => $row->Deskripsi,
	    );
            $this->sideBar();
            $this->load->view('pembayaran/pembayaran_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pembayaran'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pembayaran/create_action'),
	    'idPembayaran' => set_value('idPembayaran'),
	    'namaPetugas' => set_value('namaPetugas'),
	    'namaBank' => set_value('namaBank'),
	    'nomorRekening' => set_value('nomorRekening'),
	    'namaPemilik' => set_value('namaPemilik'),
	    'Deskripsi' => set_value('Deskripsi'),
	);
        $this->sideBar();
        $this->load->view('pembayaran/pembayaran_form', $data);
    }
    
    public function create_action() 
    {
        $nPetugas = $this->session->namaPetugas;
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'namaPetugas' => $nPetugas,
		'namaBank' => $this->input->post('namaBank',TRUE),
		'nomorRekening' => $this->input->post('nomorRekening',TRUE),
		'namaPemilik' => $this->input->post('namaPemilik',TRUE),
		'Deskripsi' => $this->input->post('Deskripsi',TRUE),
	    );

            $this->M_pembayaran->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pembayaran'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->M_pembayaran->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pembayaran/update_action'),
		'idPembayaran' => set_value('idPembayaran', $row->idPembayaran),
		'namaPetugas' => set_value('namaPetugas', $row->namaPetugas),
		'namaBank' => set_value('namaBank', $row->namaBank),
		'nomorRekening' => set_value('nomorRekening', $row->nomorRekening),
		'namaPemilik' => set_value('namaPemilik', $row->namaPemilik),
		'Deskripsi' => set_value('Deskripsi', $row->Deskripsi),
	    );
            $this->sideBar();
            $this->load->view('pembayaran/pembayaran_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pembayaran'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('idPembayaran', TRUE));
        } else {
            $data = array(
		'namaPetugas' => $this->input->post('namaPetugas',TRUE),
		'namaBank' => $this->input->post('namaBank',TRUE),
		'nomorRekening' => $this->input->post('nomorRekening',TRUE),
		'namaPemilik' => $this->input->post('namaPemilik',TRUE),
		'Deskripsi' => $this->input->post('Deskripsi',TRUE),
	    );

            $this->M_pembayaran->update($this->input->post('idPembayaran', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pembayaran'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->M_pembayaran->get_by_id($id);

        if ($row) {
            $this->M_pembayaran->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pembayaran'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pembayaran'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('namaPetugas', 'namapetugas', 'trim|required');
	$this->form_validation->set_rules('namaBank', 'namabank', 'trim|required');
	$this->form_validation->set_rules('nomorRekening', 'nomorrekening', 'trim|required');
	$this->form_validation->set_rules('namaPemilik', 'namapemilik', 'trim|required');
	$this->form_validation->set_rules('Deskripsi', 'deskripsi', 'trim|required');

	$this->form_validation->set_rules('idPembayaran', 'idPembayaran', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "pembayaran.xls";
        $judul = "pembayaran";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "NamaPetugas");
	xlsWriteLabel($tablehead, $kolomhead++, "NamaBank");
	xlsWriteLabel($tablehead, $kolomhead++, "NomorRekening");
	xlsWriteLabel($tablehead, $kolomhead++, "NamaPemilik");
	xlsWriteLabel($tablehead, $kolomhead++, "Deskripsi");

	foreach ($this->M_pembayaran->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->namaPetugas);
	    xlsWriteLabel($tablebody, $kolombody++, $data->namaBank);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nomorRekening);
	    xlsWriteLabel($tablebody, $kolombody++, $data->namaPemilik);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Deskripsi);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=pembayaran.doc");

        $data = array(
            'pembayaran_data' => $this->M_pembayaran->get_all(),
            'start' => 0
        );
        
        $this->load->view('pembayaran/pembayaran_doc',$data);
    }

}

/* End of file Pembayaran.php */
/* Location: ./application/controllers/Pembayaran.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-01-15 17:33:34 */
/* http://harviacode.com */