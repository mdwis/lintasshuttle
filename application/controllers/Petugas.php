<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class petugas extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //$this->load->model('participantmodel');
		$this->load->model('m_login');
		$this->load->model('m_tiket');

    }
	public function index()
	{
		if($this->session->userdata('logged_in')) { //Cek session login atau belum
             $role = $this->session->role;
             $idPetugas = $this->session->idPetugas;
            if ($role == 'admin') { //Cek session hak akses admin
                redirect (base_url('petugas/sideBar'));
            }else
            {
                redirect(base_url('login'));
            }
            die();
            redirect(base_url("dashboard"));
        }else {
            $data = array('alert' => false);
            $this->load->view('login/landing_page',$data);
        }
	}
	public function sideBar()
	{
		$this->load->view('Admin/_sideBar');
	}
	public function inputRute()
	{
        if($this->session->userdata('logged_in')) { //Cek session login atau belum
             $role = $this->session->role;
             $idPetugas = $this->session->idPetugas;
            if ($role == 'admin') { //Cek session hak akses admin
                $header = $this->sideBar();
                $dataTiket ['tiket'] = $this->m_tiket->getAllTiket();
                $this->load->view('Admin/insert',$dataTiket);
            }else
                {
                    redirect(base_url('login'));
                }
        }else {
            $data = array('alert' => false);
            $this->load->view('login/landing_page',$data);
        }
	}
	public function insert_rute(){
        $role = $this->session->role;
        if ($role == "admin") {
        $idPetugas = $this->session->idPetugas;
                $data = array(  
                     'Keberangkatan'          =>$this->input->post('keberangkatan'),  
                     'Tujuan'                 =>$this->input->post('tujuan'),  
                     'Stok'                   =>$this->input->post('stok'),
                     'Harga'                  =>$this->input->post('harga'),
                     'Jam'                    =>$this->input->post('jam'),
                     'idPetugas'              =>$idPetugas
                );  
                $respon = $this->m_tiket->tambahTiket($data);
                if ($respon == NULL) {
                    $success .= "<div class='alert alert-success'>
                                <strong>Success!</strong> Data Berhasil Ditambahkan.
                              </div>";
                    $this->session->set_flashdata('msg',$success);
                }else{
                    $gagal .= "<div class='alert alert-danger'>
                            <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
                            </div>";
                    $this->session->set_flashdata('msg',$success);
                }
                redirect(base_url('petugas/inputRute'));   
        }else{ 
            redirect(base_url('login'));
        }  
      }

    public function deleteId($id=" ")
    {
        $respon = $this->m_tiket->deleteTiketId($id);
           if ($respon == NULL) {
                    $success .= "<div class='alert alert-success'>
                                <strong>Success!</strong> Data Berhasil Dihapus.
                              </div>";
                    $this->session->set_flashdata('msg',$success);
                }else{
                    $gagal .= "<div class='alert alert-danger'>
                            <strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
                            </div>";
                    $this->session->set_flashdata('msg',$success);
                }
            redirect(base_url('petugas/inputRute'));
    }
    public function indexBerita()
    {   
        $this->session->set_userdata('file_manager',true);
        $this->sideBar();
        $this->load->view('Admin/insertBerita');
    }
    public function insertDataBerita()
    {
        $this->session->set_userdata('file_manager',true);
        $idPetugas = $this->session->idPetugas;
        $judul = $this->input->post('judul');
        $content = $this->input->post('content');

        $data = array(
            'judulBerita' =>$judul,
            'isiBerita'   =>$content,
            'idPetugas'   =>$idPetugas);
        $respon = $this->m_tiket->insertBerita($data);
        redirect(base_url('Petugas'));
    }  
}
