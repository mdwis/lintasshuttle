<?php
$name='Calibri-Bold';
$type='TTF';
$desc=array (
  'CapHeight' => 632,
  'XHeight' => 469,
  'FontBBox' => '[-493 -194 1239 952]',
  'Flags' => 262148,
  'Ascent' => 952,
  'Descent' => -194,
  'Leading' => 0,
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 507,
);
$unitsPerEm=2048;
$up=-87;
$ut=91;
$strp=250;
$strs=91;
$ttffile='E:/XAMPP/htdocs/sf/libraries/mpdf60/ttfonts/CalibriBold.ttf';
$TTCfontID='0';
$originalsize=264945;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='calibriB';
$panose=' 8 0 2 f 7 2 3 4 4 3 2 4';
$haskerninfo=false;
$haskernGPOS=true;
$hassmallcapsGSUB=true;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 750, -250, 221
// usWinAscent/usWinDescent = 952, -269
// hhea Ascent/Descent/LineGap = 750, -250, 221
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'cyrl' => 'DFLT SRB  ',
  'grek' => 'DFLT ',
  'latn' => 'DFLT ROM  TRK  ',
);
$GSUBFeatures=array (
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'locl' => 
      array (
        0 => 2,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
    'SRB ' => 
    array (
      'locl' => 
      array (
        0 => 2,
        1 => 3,
      ),
      'salt' => 
      array (
        0 => 3,
        1 => 17,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
  ),
  'grek' => 
  array (
    'DFLT' => 
    array (
      'locl' => 
      array (
        0 => 1,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
    'ROM ' => 
    array (
      'salt' => 
      array (
        0 => 0,
        1 => 17,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
    'TRK ' => 
    array (
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 22,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232020,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232038,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232052,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232082,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232096,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232332,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232452,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232524,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232606,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232642,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232676,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232726,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232762,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232826,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 232920,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233014,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233070,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233126,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233224,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 233856,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234432,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234530,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234548,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234566,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234638,
    ),
    'MarkFilteringSet' => '',
  ),
  25 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 234760,
    ),
    'MarkFilteringSet' => '',
  ),
  26 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235068,
    ),
    'MarkFilteringSet' => '',
  ),
  27 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235256,
    ),
    'MarkFilteringSet' => '',
  ),
  28 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235818,
    ),
    'MarkFilteringSet' => '',
  ),
  29 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 235924,
    ),
    'MarkFilteringSet' => '',
  ),
  30 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 236030,
    ),
    'MarkFilteringSet' => '',
  ),
  31 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 236048,
    ),
    'MarkFilteringSet' => '',
  ),
  32 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 236062,
    ),
    'MarkFilteringSet' => '',
  ),
  33 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 236098,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'cyrl' => 'DFLT SRB  ',
  'grek' => 'DFLT ',
  'latn' => 'DFLT ROM  TRK  ',
);
$GPOSFeatures=array (
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'SRB ' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
  'grek' => 
  array (
    'DFLT' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'ROM ' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'TRK ' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 185324,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 2,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 186212,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>