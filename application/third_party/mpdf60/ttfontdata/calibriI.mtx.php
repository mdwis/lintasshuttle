<?php
$name='Calibri-Italic';
$type='TTF';
$desc=array (
  'CapHeight' => 633,
  'XHeight' => 467,
  'FontBBox' => '[-476 -194 1214 952]',
  'Flags' => 68,
  'Ascent' => 952,
  'Descent' => -194,
  'Leading' => 0,
  'ItalicAngle' => -11,
  'StemV' => 87,
  'MissingWidth' => 507,
);
$unitsPerEm=2048;
$up=-113;
$ut=65;
$strp=250;
$strs=65;
$ttffile='D:/xampp/htdocs/sfnew/libraries/mpdf60/ttfonts/CalibriItalic.ttf';
$TTCfontID='0';
$originalsize=281272;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='calibriI';
$panose=' 8 0 2 f 5 2 2 2 4 a 2 4';
$haskerninfo=false;
$haskernGPOS=true;
$hassmallcapsGSUB=true;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 750, -250, 221
// usWinAscent/usWinDescent = 952, -269
// hhea Ascent/Descent/LineGap = 750, -250, 221
$useOTL=255;
$rtlPUAstr='';
$GSUBScriptLang=array (
  'cyrl' => 'DFLT SRB  ',
  'grek' => 'DFLT ',
  'latn' => 'DFLT ROM  TRK  ',
);
$GSUBFeatures=array (
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'locl' => 
      array (
        0 => 2,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
    'SRB ' => 
    array (
      'locl' => 
      array (
        0 => 2,
        1 => 3,
      ),
      'salt' => 
      array (
        0 => 3,
        1 => 17,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
  ),
  'grek' => 
  array (
    'DFLT' => 
    array (
      'locl' => 
      array (
        0 => 1,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
    'ROM ' => 
    array (
      'salt' => 
      array (
        0 => 0,
        1 => 17,
      ),
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 21,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
    'TRK ' => 
    array (
      'ccmp' => 
      array (
        0 => 4,
      ),
      'case' => 
      array (
        0 => 5,
      ),
      'calt' => 
      array (
        0 => 6,
        1 => 7,
      ),
      'numr' => 
      array (
        0 => 8,
      ),
      'dnom' => 
      array (
        0 => 11,
      ),
      'subs' => 
      array (
        0 => 12,
      ),
      'tnum' => 
      array (
        0 => 13,
      ),
      'pnum' => 
      array (
        0 => 14,
      ),
      'onum' => 
      array (
        0 => 15,
      ),
      'lnum' => 
      array (
        0 => 16,
      ),
      'salt' => 
      array (
        0 => 17,
      ),
      'c2sc' => 
      array (
        0 => 18,
        1 => 23,
        2 => 24,
      ),
      'smcp' => 
      array (
        0 => 19,
        1 => 20,
        2 => 22,
        3 => 23,
        4 => 24,
      ),
      'sups' => 
      array (
        0 => 25,
      ),
      'ordn' => 
      array (
        0 => 26,
      ),
      'liga' => 
      array (
        0 => 27,
      ),
      'dlig' => 
      array (
        0 => 28,
      ),
    ),
  ),
);
$GSUBLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248260,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248278,
    ),
    'MarkFilteringSet' => '',
  ),
  2 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248292,
    ),
    'MarkFilteringSet' => '',
  ),
  3 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248322,
    ),
    'MarkFilteringSet' => '',
  ),
  4 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248372,
    ),
    'MarkFilteringSet' => '',
  ),
  5 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248608,
    ),
    'MarkFilteringSet' => '',
  ),
  6 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248728,
    ),
    'MarkFilteringSet' => '',
  ),
  7 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248800,
    ),
    'MarkFilteringSet' => '',
  ),
  8 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248882,
    ),
    'MarkFilteringSet' => '',
  ),
  9 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248918,
    ),
    'MarkFilteringSet' => '',
  ),
  10 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 248952,
    ),
    'MarkFilteringSet' => '',
  ),
  11 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249002,
    ),
    'MarkFilteringSet' => '',
  ),
  12 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249038,
    ),
    'MarkFilteringSet' => '',
  ),
  13 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249102,
    ),
    'MarkFilteringSet' => '',
  ),
  14 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249196,
    ),
    'MarkFilteringSet' => '',
  ),
  15 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249290,
    ),
    'MarkFilteringSet' => '',
  ),
  16 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249346,
    ),
    'MarkFilteringSet' => '',
  ),
  17 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249402,
    ),
    'MarkFilteringSet' => '',
  ),
  18 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 249508,
    ),
    'MarkFilteringSet' => '',
  ),
  19 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 250140,
    ),
    'MarkFilteringSet' => '',
  ),
  20 => 
  array (
    'Type' => 2,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 250716,
    ),
    'MarkFilteringSet' => '',
  ),
  21 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 250814,
    ),
    'MarkFilteringSet' => '',
  ),
  22 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 250832,
    ),
    'MarkFilteringSet' => '',
  ),
  23 => 
  array (
    'Type' => 6,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 250850,
    ),
    'MarkFilteringSet' => '',
  ),
  24 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 250922,
    ),
    'MarkFilteringSet' => '',
  ),
  25 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 251044,
    ),
    'MarkFilteringSet' => '',
  ),
  26 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 251352,
    ),
    'MarkFilteringSet' => '',
  ),
  27 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 251540,
    ),
    'MarkFilteringSet' => '',
  ),
  28 => 
  array (
    'Type' => 4,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 252102,
    ),
    'MarkFilteringSet' => '',
  ),
  29 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 252208,
    ),
    'MarkFilteringSet' => '',
  ),
  30 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 252314,
    ),
    'MarkFilteringSet' => '',
  ),
  31 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 252332,
    ),
    'MarkFilteringSet' => '',
  ),
  32 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 252346,
    ),
    'MarkFilteringSet' => '',
  ),
  33 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 252382,
    ),
    'MarkFilteringSet' => '',
  ),
);
$GPOSScriptLang=array (
  'cyrl' => 'DFLT SRB  ',
  'grek' => 'DFLT ',
  'latn' => 'DFLT ROM  TRK  ',
);
$GPOSFeatures=array (
  'cyrl' => 
  array (
    'DFLT' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'SRB ' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
  'grek' => 
  array (
    'DFLT' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
  'latn' => 
  array (
    'DFLT' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'ROM ' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
    'TRK ' => 
    array (
      'cpsp' => 
      array (
        0 => 0,
      ),
      'kern' => 
      array (
        0 => 1,
      ),
    ),
  ),
);
$GPOSLookups=array (
  0 => 
  array (
    'Type' => 1,
    'Flag' => 0,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 205708,
    ),
    'MarkFilteringSet' => '',
  ),
  1 => 
  array (
    'Type' => 2,
    'Flag' => 8,
    'SubtableCount' => 1,
    'Subtables' => 
    array (
      0 => 206596,
    ),
    'MarkFilteringSet' => '',
  ),
);
?>