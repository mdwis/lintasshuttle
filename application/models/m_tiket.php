<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_tiket extends CI_Model {
    
    private $tiket = 'tiket';  

    public function getAllTiket()
    {
        $query = $this->db->order_by('idTiket','DESC')->get($this->tiket);
        return $query->result();
    }
    public function tambahTiket($data)
    {
        $this->db->insert('tiket',$data);
    }
    public function deleteTiketId($id="")
    {
        $this->db->where('idTiket', $id);
        $this->db->delete('tiket');
    }
    public function cariTiket($keberangkatan="",$tujuan="")
    {
        
        // $this->db->select('Keberangkatan');
        // $this->db->select('Tujuan');
        // $this->db->select('Harga');
        // $this->db->select('Jam');
        // $this->db->distinct();
        // $this->db->like('Keberangkatan',$keberangkatan);
        // $this->db->or_like('Tujuan',$tujuan);
        $this->db->where('Keberangkatan', $keberangkatan);
        $this->db->where('Tujuan', $tujuan);
    //      $query = $this->db->like('Keberangkatan', $keberangkatan)->order_by('idTiket')->get('tiket');
    // return $query->row();
       return $this->db->get_where('tiket')->result();
        // $query = $this->db->order_by('idTiket','ASC')->get('tiket');
        //$this->db->where('Tujuan',$tujuan);
        // return $query->result();
    }
    public function insertBerita($data="")
    {
        $this->db->insert('berita',$data);
    }
}
