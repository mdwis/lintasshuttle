<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_booking extends CI_Model {
    private $transaksi = 'transaksi';
    private $tiket     = 'tiket';

    public function insert($data,$insert_id= "")
    {
        $this->db->insert($this->transaksi,$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function getDataTiket()
    {
        $this->db->distinct();
        $query = $this->db->get($this->tiket);
        return $query->result();
    }
    public function updateJam($id ="",$data="")
    {
        $this->db->where('idTransaksi',$id);
        $insert_id = $this->db->insert_id();
        $this->db->update('transaksi',$data);
        return $id;
    }
    public function updateKursi($id ="",$data="")
    {
        $this->db->where('idTransaksi',$id);
        $this->db->update('transaksi',$data);
        return $id;
    }
    public function getAllTransaksiId($id='',$data="")
    {
        $this->db->where('idTransaksi',$id);
        return $this->db->update($this->transaksi,$data);
    }
    // public function getTransaksiId($where)
    // {
    //     $query = $this->db
    //     ->where($where)
    //     ->get($this->table);
    // }
    public function getAllTransaksi()
    {
        $query = $this->db->get($this->transaksi);
        return $query->row();
    }
    public function getDataId($id)
    {
        $this->db->where('idTransaksi',$id);
        return $this->db->get($this->transaksi)->row();
    }
    public function getDataTiketId($id)
    {
        $this->db->where('idTransaksi',$id);
        return $this->db->get($this->transaksi)->row();
    }
}
