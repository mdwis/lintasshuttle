<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_penumpang extends CI_Model {
    
    public function insertPenumpang($data="",$id="")
    {

        $this->db->insert('penumpang',$data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
    public function getPenumpangId($id="")
    {
        $this->db->where('idPenumpang',$id);
        return $this->db->get('penumpang')->row();
    }
    public function penumpangId($idPenumpang = 0)
    {
       $sql = "SELECT * FROM penumpang        
       WHERE idPenumpang = ?";
        $result = $this->db->query($sql, array($idPenumpang));
        return $result->row_array();
    }
    public function getAllPenumpangTransaction($id="") {
       $sql =("SELECT * FROM penumpang 
        INNER JOIN transaksi 
        ON penumpang.IdTransaksi = transaksi.idTransaksi
        WHERE idPenumpang = $id");

       $sqltiga = ("SELECT * FROM penumpang 
INNER JOIN transaksi 
        ON penumpang.IdTransaksi = transaksi.idTransaksi
INNER JOIN tiket
  ON transaksi.idTiket = tiket.idTiket
WHERE idPenumpang = $id");  

        $result = $this->db->query($sql);
        return $result->result();
    }
     public function getIdPenumpangTransaction() {
      $this->db->select('*');
      $this->db->join('transaksi','penumpang.idTransaksi = transaksi.idTransaksi');
      $this->db->from('penumpang');
      $this->db->where('idTransaksi',45);
      $this->db->where('table1.col1', 2);
      // $this->db->join('tbl_role', 'tbl_user.id_role = tbl_role.id');
      // $this->db->from('tbl_user');
      $query = $this->db->get();
      return $query->row();
    }
    public function readpesanan($table,$where)
    {
      return $this->db->get_where($table, $where);
    }

}
