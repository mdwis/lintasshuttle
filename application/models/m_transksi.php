<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_transksi extends CI_Model {
    
    private $transaksi = 'transaksi';  

    public function getAllTransaksi()
    {
        $query = $this->db->get('transaksi');
        return $query->row();
    }
    public function getTrans()
    {
        $query = $this->db->get($this->transaksi);
        return $query->result();
    }
    public function getTran($id="")
    {
        $this->db->where('idTransaksi',$id);
        return $this->db->get($this->transaksi)->row();
    }

}
