<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_login extends CI_Model {

	function Login($username,$password){
        if($username=="" || $password=="")
            return null;
        $this->security->xss_clean($username);
        //$username=xss_clean($username);
        $password=md5($password);

        $sql = "SELECT * FROM user";
        $result = $this->db->query($sql, array($username, $password));
        //echo $this->db->last_query();
        return $result->result_array();
    }
    function CekAktivasiUser($username='')
    {
        $this->security->xss_clean($username);
        $sql = "SELECT * FROM user WHERE EMAIL = ?";
        $result = $this->db->query($sql, array($username));
        return $result->result_array();
    }
    function validate_loginUser($postData)
    {

        //$query = $this->db->get('petugas');
        //return $query->row();

        $this->db->select('*');
        $this->db->where('username', $postData['username']);
        $this->db->where('password', $postData['password']);
        $this->db->where('status', 1);
        $this->db->from('penumpang');
        $query = $this->db->get();
        // print_r($query->result_array());die();
        if ($query->num_rows() == 0)
            return [false,null];
        else
            return [true,$query->row()];
            //return $query->row();

    }
    function validate_login($postData){

        //$query = $this->db->get('petugas');
        //return $query->row();

        $this->db->select('*');
        $this->db->where('username', $postData['username']);
        $this->db->where('password', $postData['password']);
        $this->db->where('status', 1);
        $this->db->from('petugas');
        $query = $this->db->get();
        // print_r($query->result_array());die();
        if ($query->num_rows() == 0)
            return [false,null];
        else
            return [true,$query->row()];
            //return $query->row();
    }
    function cek_pengguna(){        
    $username = $this->input->post('username');
    $password = $this->input->post('password');
    return $this->db->get_where('penumpang',array('username'=>$username,'password'=>$password));
    }   

}
