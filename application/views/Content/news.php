<div class="container">
	<!-- Page Heading -->
      <strong><h1 class="my-4">Berita dan Promo</h1></strong>

        <div class="row">
          <?php foreach($data as $berita) {?>
            <div class="col-md-12"><?php echo $berita->tglBerita; ?></div>
            <div class="row">
            <div class="col-md-10">
              <strong><?php echo $berita->judulBerita; ?></strong>
              <p><?php echo $berita->isiBerita; ?></p>
              <p class="text-right"><?php echo $berita->penulis; ?></p>
            </div>    
        </div>
        <?php }?>
</div>