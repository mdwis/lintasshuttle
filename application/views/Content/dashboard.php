<!DOCTYPE html>
<html>
<head>
  <title></title>
   <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
   <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>
     <style>
  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;
  }
  </style>
</head>
<body>
                <div id="demo" class="carousel slide" data-ride="carousel">

                  <!-- Indicators -->
                  <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                  </ul>
                  
                  <!-- The slideshow -->
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="<?php echo base_url('images/1.jpg'); ?>" alt="Los Angeles" width="1100" height="500">
                    </div>
                    <div class="carousel-item">
                      <img src="<?php echo base_url('images/2.jpg'); ?>" alt="Chicago" width="1100" height="500">
                    </div>
                    <div class="carousel-item">
                      <img src="<?php echo base_url('images/3.jpg'); ?>" alt="New York" width="1100" height="500">
                    </div>
                  </div>
                  
                  <!-- Left and right controls -->
                  <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>
                </div>




    <main role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron">
        <div class="container">
          <h1 class="display-3">PESAN SEKARANG !!!</h1>
          <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
          <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
        </div>
      </div>

      <div class="container">
        <!-- Example row of columns -->
        <div class="row">
          <div class="col-md-4">
            <h2>Litas Shuttle</h2>
            <img src="<?php echo base_url('images/shuttle.png'); ?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
            <p>Layanan Shuttle merupakan layanan angkutan darat yang menghubungkan antar kota dan atau antar provinsi dengan sistem Point to Point Service, dimana Pelanggan akan berangkat dari titik yang sudah ditentukan dan tiba di titik tujuan yang sudah ditentukan pula, berdasarkan jadwal regular yang tersedia </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
          <div class="col-md-4">
            <h2>Heading</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
          </div>
        </div>

        <hr>

      </div> <!-- /container -->

    </main>


<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 mt-3">

          <!-- Content -->
          <h5 class="text-uppercase">Tentang Kami</h5>
          <p>Layanan Shuttle merupakan layanan angkutan darat yang menghubungkan antar kota dan atau antar provinsi dengan sistem Point to Point Service, dimana Pelanggan akan berangkat dari titik yang sudah ditentukan dan tiba di titik tujuan yang sudah ditentukan pula, berdasarkan jadwal regular yang tersedia </p>

        </div>
        <!-- Grid column -->

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
        <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">Ikuti Kami</h5>

            <ul class="list-unstyled">
              <li>
                <img src="<?php echo base_url('images/igg.png'); ?>"><a href="#!">@lintasshullte</a>
              </li>
              <li>
                <img src="<?php echo base_url('images/fb.png');?>"><a href="#!">Lintas Shuttlle </a>
              </li>
              <li>
                <img src="<?php echo base_url('images/tw.png');?>"><a href="#!">Lintas Shuttle</a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">alamat</h5>
            <p>Jl. Cipaganti No. 164 Bandung 40131 Indonesia
Non Reservasi (022) 2031824 </p>

          </div>
          <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="https://lintas-shuttle.co.id/"> LintasShuttle.com</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->

</body>
</html>