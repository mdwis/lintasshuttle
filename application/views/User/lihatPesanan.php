          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Data Transaksi Penumpang</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal Keberangkatan</th>
                      <th>Jumlah</th>
                      <th>Keberangkatan</th>
                      <th>Tujuan</th>
                      <th>Jam</th>
                      <th>Tempat Duduk</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($transaksi as $row ) { ?>
                    <tr>
                      <th><?php echo $row->idTransaksi; ?></th>
                      <th><?php echo $row->tanggalKeberangkatan; ?></th>
                      <th><?php echo $row->jumlah; ?></th>
                      <th><?php echo $row->Keberangkatan; ?></th>
                      <th><?php echo $row->Tujuan; ?></th>
                      <th><?php echo $row->Jam; ?></th>
                      <th><?php echo $row->tempatDuduk; ?></th>
                     <?php  } ?>
                    </tr>
                  </tbody>
                </table>
              </div>
    </div>
  </div>