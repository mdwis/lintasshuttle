<div class="form-book">
                        <form id="formHome" method="post" action="<?php echo base_url('Booking/insert'); ?>">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Keberangkatan</label>
                                    <select class="form-control" id="keberangkatan" name="keberangkatan" required="required">
                                        <option>Pilih Kota</option>
                                        <?php foreach($tiket as $berangkat): ?>
                                        <option value="<?php echo $berangkat->Keberangkatan; ?>"><?php echo $berangkat->Keberangkatan; ?></option>
                                        <?php endforeach; ?>
                                        <option>Bandung</option>
                                    </select>
                                    <label for="keberangkatan" generated="true" class="error"></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Tujuan</label>
                                    <select class="form-control" id="tujuan" name="tujuan" required="required">
                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Pilih Lokasi</option>
                                    </select>
                                    <label for="tujuan" generated="true" class="error"></label>
                                </div>
                                 <!-- // Rute Gak ada
                                 <div class="form-group col-md-12">
                                    <label>Rute</label>
                                    <select class="form-control rute-input" id="rute" name="rute" required="required">
                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Pilih Rute</option>
                                    </select>
                                    <label for="rute" generated="true" class="error"></label>
                                    <label class="rute-error"></label>
                                </div> 
                            -->
                                <div class="col-md-12 text-center margintop20">
                                    <button class="btn btn-primary" >Order</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Form -->
                </div>


                      <script type="text/javascript">
        $(document).ready(function(){
           $('#keberangkatan').on('change', function(){
                var Keberangkatan = $(this).val();
                if(Keberangkatan == '')
                {
                    $('#tujuan').prop('disabled',true);
                }else
                    {
                       $('#tujuan').prop('disabled',false);
                      $.ajax({
                        url:"<?php echo base_url() ?>Booking/ajaxGetTujuan",
                        type: "POST",
                        data: {'Keberangkatan' : Keberangkatan},
                        dataType: 'json',
                        success: function(data){
                           $('#tujuan').html(data);
                        },
                        error: function(){
                            alert('Error occur...!!');
                        }
                            }); 
                    }
           }); 
        });
    </script>
    <!-- End Select Box -->
        <script type="text/javascript">
        $(document).ready(function(){
           $('#jadwal').on('change', function(){                
                var Keberangkatan = $(this).val();
                 $.ajax({  
                    url:"<?php echo base_url() ?>Booking/ajaxGetJadwal",  
                    method:"POST",  
                    data:{'Keberangkatan':Keberangkatan}, 
                    success:function(data){
                        //$("#schedule").html("C"); ->Untuk ngecek ketika onChange
                         $('#hasil').html(data);
                    }
                });     
           }); 
        });
    </script>
