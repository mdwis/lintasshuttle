<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

    <title>L I N T A S | Shuttle</title>

    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
    <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/favicon.css');?>" integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
     <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css?fbclid=IwAR3D17_Dv_8ub4h3KYnUucKv-WJmI9hxy6xd60Dx9wvZp-uOlkD-X0jGhys"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js?fbclid=IwAR2dAyQBdpHjJ6iwfeZNBmcwAENI_aqzhpDuSAL_XptbSOyXVG35PmAV0T8"></script>
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css?fbclid=IwAR3JktE0U22wfjcrQRZQ2iioHDiZHnSs5HIQbdJciIJxGVQFG41rxgS0p98">


  <style>
  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;
  }
  </style>
  </head>

  <body>

                <div id="demo" class="carousel slide" data-ride="carousel">

                  <!-- Indicators -->
                  <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                  </ul>
                  
                  <!-- The slideshow -->
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img src="<?php echo base_url('images/1.jpg'); ?>" alt="Los Angeles" width="1100" height="500">
                    </div>
                    <div class="carousel-item">
                      <img src="<?php echo base_url('images/2.jpg'); ?>" alt="Chicago" width="1100" height="500">
                    </div>
                    <div class="carousel-item">
                      <img src="<?php echo base_url('images/3.jpg'); ?>" alt="New York" width="1100" height="500">
                    </div>
                  </div>
                  
                  <!-- Left and right controls -->
                  <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                  </a>
                </div>


<!-- Footer -->
<footer class="page-footer font-small blue pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 mt-3">

          <!-- Content -->
          <h5 class="text-uppercase">Tentang Kami</h5>
          <p>Layanan Shuttle merupakan layanan angkutan darat yang menghubungkan antar kota dan atau antar provinsi dengan sistem Point to Point Service, dimana Pelanggan akan berangkat dari titik yang sudah ditentukan dan tiba di titik tujuan yang sudah ditentukan pula, berdasarkan jadwal regular yang tersedia </p>

        </div>
        <!-- Grid column -->

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
        <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">Ikuti Kami</h5>

            <ul class="list-unstyled">
              <li>
                <img src="<?php echo base_url('images/igg.png'); ?>"><a href="#!">@lintasshullte</a>
              </li>
              <li>
                <img src="<?php echo base_url('images/fb.png');?>"><a href="#!">Lintas Shuttlle </a>
              </li>
              <li>
                <img src="<?php echo base_url('images/tw.png');?>"><a href="#!">Lintas Shuttle</a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">alamat</h5>
            <p>Jl. Cipaganti No. 164 Bandung 40131 Indonesia
Non Reservasi (022) 2031824 </p>

          </div>
          <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
      <a href="https://lintas-shuttle.co.id/"> LintasShuttle.com</a>
    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->



    <script src="<?php echo base_url('bootstrap/js/jquery-3.3.1.slim.min.js');?>" ></script>
    <script src="<?php echo base_url('bootstrap/js/popper.min.js');?>"></script>
  </body>
</html>
