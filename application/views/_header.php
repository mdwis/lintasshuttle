<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

    <title>L I N T A S | Shuttle</title>

    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
    <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/favicon.css');?>" integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
     <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
  </head>

  <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <!-- Logo -->
            <img src="<?php echo base_url('images/lintas.png');?>" width="100" height="50" >

            <!-- Menu -->
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
              <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url();?>">Home <span class="sr-only">(current)</span><i style='font-size:24px' class='fas'>&#xf015;</i></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url(); ?>Booking/">Booking <span class="sr-only">(current)</span><i style='font-size:24px' class='fas'>&#xf207;</i></a> 
              </li>

              <li class="nav-item active">
                <a class="nav-link" href="#">Berita dan Promo <span class="sr-only">(current)</span><i style="font-size:24px" class="fa">&#xf1ea;</i></a>
              </li>

              <li class="nav-item active">
                <a class="nav-link" href="#">Jadwal Keberangkatan <span class="sr-only">(current)</span><i style='font-size:24px' class='fas'>&#xf073;</i></a>
              </li>
            </ul>

              <button type="button" class="btn btn-primary">Daftar</button>
              &emsp;
              <a href="<?php echo base_url()?>login" class="btn btn-primary" role="button">Masuk</a>
          </div>
        </nav>
    <script src="<?php echo base_url('bootstrap/js/jquery-3.3.1.slim.min.js');?>" ></script>
    <script src="<?php echo base_url('bootstrap/js/popper.min.js');?>"></script>

  </body>
</html>
