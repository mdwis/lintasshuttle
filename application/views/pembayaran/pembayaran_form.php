    <div class="container">
        <h2 style="margin-top:0px">Pembayaran <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
        <div class="form-group">
            <!-- <label for="varchar">NamaPetugas <?php echo form_error('namaPetugas') ?></label> -->
            <input type="hidden" class="form-control" name="namaPetugas" id="namaPetugas" placeholder="NamaPetugas" value="<?php echo $nPetugas = $this->session->namaPetugas; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">NamaBank <?php echo form_error('namaBank') ?></label>
            <select class="form-control" id="sel1" name="namaBank" id="namaBank" value="<?php echo $namaBank; ?>">
                <option value="BANK BCA">BANK BCA </option>   
                <option value="BANK MANDIRI">BANK MANDIRI   </option> 
                <option value="BANK BNI">BANK BNI    </option>
                <option value="BANK BNI SYARIAH">BANK BNI SYARIAH    </option>
                <option value="BANK BRI">BANK BRI    </option>
                <option value="BANK SYARIAH MANDIRI">BANK SYARIAH MANDIRI </option>   
                <option value="BANK CIMB NIAGA">BANK CIMB NIAGA     </option>
                <option value="BANK CIMB NIAGA SYARIAH">BANK CIMB NIAGA SYARIAH     </option>
                <option value="BANK MUAMALAT">BANK MUAMALAT   </option>
                <option value="BANK BRI SYARIAH">BANK BRI SYARIAH    </option>
                <option value="BANK TABUNGAN NEGARA (BTN)">BANK TABUNGAN NEGARA (BTN)  </option>
                <option value="PERMATA BANK">PERMATA BANK    </option>
                <option value="BANK DANAMON">BANK DANAMON    </option>
                <option value="BANK BII MAYBANK ">BANK BII MAYBANK    </option>
                <option value="BANK MEGA">BANK MEGA   </option>
                <option value="BANK SINARMAS">BANK SINARMAS   </option>
                <option value="BANK COMMONWEALTH">BANK COMMONWEALTH   </option>
                <option value="BANK OCBC NISP">BANK OCBC NISP  </option>
                <option value="BANK BUKOPIN">BANK BUKOPIN    </option>
                <option value="BANK BCA SYARIAH">BANK BCA SYARIAH    </option>
                <option value="BANK LIPPO">BANK LIPPO  </option>
                <option value="CITIBANK">CITIBANK    </option>
                <option value="BANK TABUNGAN PENSIUNAN NASIONAL (BTPN)">BANK TABUNGAN PENSIUNAN NASIONAL (BTPN) </option>
            </select>
        </div>
	    <div class="form-group">
            <label for="bigint">NomorRekening <?php echo form_error('nomorRekening') ?></label>
            <input type="text" class="form-control" name="nomorRekening" id="nomorRekening" placeholder="NomorRekening" value="<?php echo $nomorRekening; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">NamaPemilik <?php echo form_error('namaPemilik') ?></label>
            <input type="text" class="form-control" name="namaPemilik" id="namaPemilik" placeholder="NamaPemilik" value="<?php echo $namaPemilik; ?>" />
        </div>
	    <div class="form-group">
            <label for="Deskripsi">Deskripsi <?php echo form_error('Deskripsi') ?></label>
            <textarea class="form-control" rows="3" name="Deskripsi" id="Deskripsi" placeholder="Deskripsi"><?php echo $Deskripsi; ?></textarea>
        </div>
	    <input type="hidden" name="idPembayaran" value="<?php echo $idPembayaran; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pembayaran') ?>" class="btn btn-default">Cancel</a>
	</form>
</div>