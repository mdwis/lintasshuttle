    <div class="container">
        <h2 style="margin-top:0px">Pembayaran List</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('pembayaran/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('pembayaran/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('pembayaran'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>NamaPetugas</th>
		<th>NamaBank</th>
		<th>NomorRekening</th>
		<th>NamaPemilik</th>
		<th>Deskripsi</th>
		<th>Action</th>
            </tr><?php
            foreach ($pembayaran_data as $pembayaran)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $pembayaran->namaPetugas ?></td>
			<td><?php echo $pembayaran->namaBank ?></td>
			<td><?php echo $pembayaran->nomorRekening ?></td>
			<td><?php echo $pembayaran->namaPemilik ?></td>
			<td><?php echo $pembayaran->Deskripsi ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('pembayaran/read/'.$pembayaran->idPembayaran),'Read'); 
				echo ' | '; 
				echo anchor(site_url('pembayaran/update/'.$pembayaran->idPembayaran),'Update'); 
				echo ' | '; 
				echo anchor(site_url('pembayaran/delete/'.$pembayaran->idPembayaran),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
		<?php echo anchor(site_url('pembayaran/excel'), 'Excel', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('pembayaran/word'), 'Word', 'class="btn btn-primary"'); ?>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </div>