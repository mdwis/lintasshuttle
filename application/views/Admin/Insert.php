
        <div class="container" style="margin-top: 30px">
    <div class="col-md-12">
      <?php echo $this->session->flashdata('msg'); ?>
        <h2 style="text-align: center;margin-bottom: 30px"> Data Rute</h2>
        <button type="button" data-toggle="modal" data-target="#userModal" class="btn btn-info btn-lg">Tambah Rute</button>  
         <br /><br />
        <table id="table_id" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th> No</th>
                    <th> Keberangkatan</th>
                    <th> Tujuan</th>
                    <th> Stok</th>
                    <th> Harga</th>
                    <th> Jam</th>
                    <th style="width: 125px;"> Action</p> </th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($tiket as $tik) {
                ?>
                <tr>
                    <td><?php echo $tik->idTiket; ?></td>
                    <td><?php echo $tik->Keberangkatan; ?></td>
                    <td><?php echo $tik->Tujuan; ?></td>
                    <td><?php echo $tik->Stok; ?></td>
                    <td><?php echo $tik->Harga; ?></td>
                    <td><?php echo $tik->Jam; ?></td>
                    <td style="text-align: center;">

                        <button class="btn btn-sm btn-primary" onclick="edit_book(<?php base_url()?>'Edit/id')"><i class="glyphicon glyphicon-pencil"></i>Edit</button>
                        <!-- <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal" value="<?php echo base_url('Petugas/deleteId/'.$tik->idTiket); ?>"><a href="<?php echo base_url('Petugas/deleteId/'.$tik->idTiket); ?>"></a><i style="font-size:24px" class="fa">&#xf1f8;</i>Delete</button> -->
                        <?php echo anchor('Petugas/deleteId/'. $tik->idTiket,'Hapus'); ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin ingin menghapus ?</p>
        </div>
        <div class="modal-footer">
          <a href="<?php echo base_url('Petugas/deleteId/'.$tik->idTiket); ?>" class="btn btn-sm btn-primary">Ya</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        </div>
      </div>
      
    </div>
  </div>



<script type="text/javascript">
      $(document).ready( function () {
          $('#table_id').DataTable();
      } );
    </script>
    <div id="userModal" class="modal fade">
        <div class="modal-dialog">

            <form action="<?php echo base_url('petugas/insert_rute');?>" method="post" id="user_form" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-tittle"></h4>
                    </div>
                    <div class="modal-body">
                        <label>Keberangkatan</label>
                        <input type="text" name="keberangkatan" id="keberangkatan" class="form-control">
                        <label>Tujuan</label>
                        <input type="text" name="tujuan" id="tujuan" class="form-control">
                        <label>Stok</label>
                        <input type="text" name="stok" id="stok" class="form-control">
                        <label>Harga</label>
                        <input type="text" name="harga" id="harga" class="form-control">
                        <label>Jam</label>
                        <input type="time" name="jam" id="harga" min="5:00" max="22:00" class="form-control">
                    <div class="modal-footer">
                      <input type="submit" name="" value="Tambah">
                      </form>
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>          
        </div>
    <!-- <script type="text/javascript" language="javascript" >  
 $(document).ready(function(){  
      var dataTable = $('#user_data').DataTable({  
           "processing":true,  
           "serverSide":true,  
           "order":[],  
           "ajax":{  
                url:"<?php echo base_url() . 'crud/fetch_user'; ?>",  
                type:"POST"  
           },  
           "columnDefs":[  
                {  
                     "targets":[0, 3, 4],  
                     "orderable":false,  
                },  
           ],  
      });  
      $(document).on('submit', '#user_form', function(event){  
           event.preventDefault();  
           var keberangkatan = $('#keberangkatan').val();  
           var tujuan = $('#tujuan').val();
           var stok = $('#stok').val(); 
           var harga = $('#harga').val();  
           var jam = $('#jam').val();  
           if(keberangkatan != '' && tujuan != '')  
           {  
                $.ajax({  
                     url:"<?php echo base_url() . 'petugas/user_action'?>",  
                     method:'POST',  
                     data:new FormData(this),  
                     contentType:false,  
                     processData:false,  
                     success:function(data)  
                     {  
                          alert(data);  
                          $('#user_form')[0].reset();  
                          $('#userModal').modal('hide');  
                     }  
                });  
           }  
           else  
           {  
                alert("Bother Fields are Required");  
           }  
      });  
 });  
 </script> -->