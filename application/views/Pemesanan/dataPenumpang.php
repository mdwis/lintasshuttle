<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
    <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>


    <!-- Icon -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/favicon.css');?>" integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
     <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/_v3/css/style.css');?>" />

    <!--[if lt IE 9]><script th:src="${PROJECT.get('jsUrl')} + @{/online/vendor/ie8-responsive-file-warning.js}"></script><![endif]-->
    <script src="<?php echo base_url('bootstrap/_v3/css/ie-emulation-modes-warning.js');?>"></script>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css?fbclid=IwAR3D17_Dv_8ub4h3KYnUucKv-WJmI9hxy6xd60Dx9wvZp-uOlkD-X0jGhys"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js?fbclid=IwAR2dAyQBdpHjJ6iwfeZNBmcwAENI_aqzhpDuSAL_XptbSOyXVG35PmAV0T8"></script>
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css?fbclid=IwAR3JktE0U22wfjcrQRZQ2iioHDiZHnSs5HIQbdJciIJxGVQFG41rxgS0p98">
    <!--[if lt IE 9]>
    <script th:src="${PROJECT.get('jsUrl')} + @{/online/vendor/html5shiv.min.js}"></script>
    <script th:src="${PROJECT.get('jsUrl')} + @{/online/vendor/respond.min.js}"></script>
    <![endif]-->
</head>
<body>

<div class="clearfix"></div>
    
    <!-- Start Section -->
    <div id="section-home" class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-left">
                    <h4 class="marginbot25" align="center">Booking Book</h4>
                    
                    <!-- Start Form -->
                    <div class="form-book">
                        <form id="formHome" method="POST" action="<?php echo base_url('Booking/cari'); ?>">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>Keberangkatan</label>
                                    <select class="form-control" id="keberangkatan" name="keberangkatan" required="required">
                                        <option>Pilih Kota</option>
                                        <?php foreach($tiket as $berangkat): ?>
                                        <option value="<?php echo $berangkat->Keberangkatan; ?>"><?php echo $berangkat->Keberangkatan; ?></option>
                                        <?php endforeach; ?>
                                        <option>Bandung</option>
                                    </select>
                                    <label for="keberangkatan" generated="true" class="error"></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Tujuan</label>
                                    <select class="form-control" id="tujuan" name="tujuan" required="required">
                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Pilih Kota</option>
                                    </select>
                                    <label for="tujuan" generated="true" class="error"></label>
                                </div>
                                 <!-- // Rute Gak ada
                                 <div class="form-group col-md-12">
                                    <label>Rute</label>
                                    <select class="form-control rute-input" id="rute" name="rute" required="required">
                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Pilih Rute</option>
                                    </select>
                                    <label for="rute" generated="true" class="error"></label>
                                    <label class="rute-error"></label>
                                </div> 
                            -->
                                <div class="form-group col-md-8">
                                <label>Date</label>
                                    <input type="text" name="tanggalKeberangkatan" class="datepicker form-control"  id="datepicker">
                                        <script>
                                        $('.datepicker').datepicker({
                                        startDate: '0d',
                                        format: "yyyy-mm-dd",
                                        todayBtn: true,
                                        todayHighlight: true,
                                        autoclose: 1,
                                        endDate: '+7d',
                                        });
                                        </script>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Jumlah</label>
                                    <input type="number" class="form-control number" placeholder="0" value="1" min="1" max="11" id="jumlah" name="jumlah" required="required" />
                                </div>
                              <!--   <div class="form-group col-md-4">
                                    <label>Jumlah</label>
                                    <input type="number" class="form-control number" placeholder="0" value="1" min="1" max="11" id="jumlah" name="jumlah" required="required" />
                                </div> -->
                                <div class="col-md-12 text-center margintop20">
                                    <input type="submit" class="btn btn-primary" value="Cari">
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Form -->
                </div>
                <div class="col-md-7 col-right">
                    <!-- Start Form Title -->
                    <div class="input-group form-title">
                        <label class="input-group-addon">Hasil Pencarian</label>
<!--                         <select class="form-control" id="jadwal">
                            <option>Lihat Jadwal</option>
                            <?php foreach($tiket as $berangkat): ?>
                            <option value="<?php echo $berangkat->Keberangkatan; ?>"><?php echo $berangkat->Keberangkatan; ?></option>
                            <?php endforeach; ?>
                        </select> -->
                    </div>
                    <!-- End Form Title -->
                    <!-- Start Table -->
                    <table class="table-result" >
                        <thead>
                            <tr>
                                <th>Keberangkatan</th>
                                <th>Tujuan</th>
                                <th>Harga</th>
                                <th>Jam</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="hasil">
                            <tr>
                                <!-- <td colspan="5" class="text-center">
                                    <img alt="Loading" style="width:25px" src="https://booking.cititrans.co.id/static/image/loading.gif" /> Loading...
                                </td> -->
                            <?php 
                            foreach ($hasil as $cari) { ?>
                                <td><?php echo $cari->Keberangkatan ?></td>
                                <td><?php echo $cari->Tujuan ?></td>
                                <td><?php echo $cari->Harga ?></td>
                                <td><?php echo $cari->Jam ?></td>
                               <!--  <?php 
                                 echo "<td>".$row->Keberangkatan."</td>"; 
                                 echo "<td>".$row->Tujuan."</td>"; 
                                 echo "<td>".$row->Harga."</td>"; 
                                 echo "<td>".$row->Jam."</td>";
                                 ?> -->
                                 <?php echo "<td>".anchor("Booking/insertTransaksi/".$cari->idTiket."/".$cari->Keberangkatan."/".$cari->Tujuan."/".$cari->Harga."/".$cari->Jam,"Pilih"); ?>
                                
                                
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <!-- End Table -->
                    <p>
                        Note: Schedule can be changed due to force majour or conditions apply.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Section -->

<!-- START JAVASCRIPT -->

    <script src="<?php echo base_url('bootstrap/_v3/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('bootstrap/_v3/js/bootstrap.min.js'); ?>"></script>
    <!-- END JAVASCRIPT -->
    <!-- Start Select Box -->
        <script type="text/javascript">
        $(document).ready(function(){
           $('#keberangkatan').on('change', function(){
                var Keberangkatan = $(this).val();
                if(Keberangkatan == '')
                {
                    $('#tujuan').prop('disabled',true);
                }else
                    {
                       $('#tujuan').prop('disabled',false);
                      $.ajax({
                        url:"<?php echo base_url() ?>Booking/ajaxGetTujuan",
                        type: "POST",
                        data: {'Keberangkatan' : Keberangkatan},
                        dataType: 'json',
                        success: function(data){
                           $('#tujuan').html(data);
                        },
                        error: function(){
                            alert('Error occur...!!');
                        }
                            }); 
                    }
           }); 
        });
    </script>
    <!-- End Select Box -->
        <script type="text/javascript">
        $(document).ready(function(){
           $('#jadwal').on('change', function(){                
                var Keberangkatan = $(this).val();
                 $.ajax({  
                    url:"<?php echo base_url() ?>Booking/ajaxGetJadwal",  
                    method:"POST",  
                    data:{'Keberangkatan':Keberangkatan}, 
                    success:function(data){
                        //$("#schedule").html("C"); ->Untuk ngecek ketika onChange
                         $('#hasil').html(data);
                    }
                });     
           }); 
        });
    </script>


</body>
</html>
