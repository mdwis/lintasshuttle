<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
    <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>


    <!-- Icon -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/favicon.css');?>" integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
     <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/_v3/css/style.css');?>" />

    <!--[if lt IE 9]><script th:src="${PROJECT.get('jsUrl')} + @{/online/vendor/ie8-responsive-file-warning.js}"></script><![endif]-->
    <script src="<?php echo base_url('bootstrap/_v3/css/ie-emulation-modes-warning.js');?>"></script>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css?fbclid=IwAR3D17_Dv_8ub4h3KYnUucKv-WJmI9hxy6xd60Dx9wvZp-uOlkD-X0jGhys"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js?fbclid=IwAR2dAyQBdpHjJ6iwfeZNBmcwAENI_aqzhpDuSAL_XptbSOyXVG35PmAV0T8"></script>
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css?fbclid=IwAR3JktE0U22wfjcrQRZQ2iioHDiZHnSs5HIQbdJciIJxGVQFG41rxgS0p98">
    <!--[if lt IE 9]>
    <script th:src="${PROJECT.get('jsUrl')} + @{/online/vendor/html5shiv.min.js}"></script>
    <script th:src="${PROJECT.get('jsUrl')} + @{/online/vendor/respond.min.js}"></script>
    <![endif]-->
</head>
<body>
  <br>
  <br>
  <br>
  <br>
<div class="container">
<div class="col-md-9 col-right">
                    <!-- Start Table -->
                                                         
                    <form action="<?php echo base_url('Booking/updateJam/'.$transaksi->idTransaksi);?>" method="POST">
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Example select</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="jam">
                          <?php foreach($tiket as $berangkat): ?>
                          <option value="<?php echo $berangkat->Jam; ?>"><?php echo $berangkat->Jam; ?></option>
                          <?php endforeach; ?>
                        </select>
                        
                      </div>
                      <a class="btn btn-primary" href="<?Php echo base_url('Booking/updateJam/'.$transaksi->idTransaksi);?>" role="button">Pilih</a>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    
                    <!-- End Table -->
                </div>
            </div>
        </div>
    </div>
  </div>
    <!-- End Section -->
<!-- START JAVASCRIPT -->
    <script src="<?php echo base_url('bootstrap/_v3/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('bootstrap/_v3/js/bootstrap.min.js'); ?>"></script>
    <!-- END JAVASCRIPT -->

</body>
</html>