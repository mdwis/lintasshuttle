<!DOCTYPE html>
<html>
<head>
    <title></title>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>

<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-xs-10">
            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <h2 class="panel-title">
                         <?php foreach($data as $dat){  ?>
                        Accounts and transactions report
                    </h2>
                    <a href='<?php echo base_url('Booking/send/');echo $dat->idPenumpang?>'
                     class="btn btn-default" role="button"><span class="glyphicon glyphicon-print">Cetak</a></span>
                </div>
                <div class="panel-body">
                    <h3>
                         <?php echo $dat->namaPenumpang; ?>
                    </h3>
                </div>
                    <ul class="list-group">
                    <li class="list-group-item">
                     <h4>Nomor Transaksi # <?php echo $dat->idTransaksi; ?></h4> 
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Tanggal Keberangkatan </th>
                                <th>Jumlah</th>
                                <th>Keberangkatan</th>
                                <th>Tujuan</th>
                                <th>Jam</th>
                                <th>Nomor Kursi</th>
                            </tr>
                        </thead>
                        <tbody>
                             
                            <tr>
                                <td><?php echo $dat->tanggalKeberangkatan; ?></td>
                                <td><?php echo $dat->jumlah; ?></td>
                                <td><?php echo $dat->t_keberangkatan;?></td>
                                <td><?php echo $dat->t_tujuan;?></td>
                                <td><?php echo $dat->t_jam;?></td>
                                <td><?php echo $dat->tempatDuduk;?></td>
                            </tr>
                            <!--  <tr>
                                <td><?php echo $join->tanggalKeberangkatan; ?></td>
                                <td><?php echo $join->jumlah; ?></td>
                                <td><?php echo $join->keberangkatan;?></td>
                                <td><?php echo $join->tujuan;?></td>
                                <td><?php echo $join->jam;?></td>
                                <td><?php echo $join->tempatDuduk;?></td>
                            </tr> -->
                       <!-- <?php }?> -->
                        </tbody>
                    </table>
                    <?php 
                    $t_harga    = $dat->t_harga;
                    $t_jumlah   = $dat->jumlah;
                    $hasil = $t_harga*$t_jumlah;
                    ?>
                    <h3 align="right">Subtotal</h3>
                    <h4 align="right"><?php echo $hasil; ?></h4>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

</body>
</html>