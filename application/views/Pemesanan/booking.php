<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">

    <title>L I N T A S | Shuttle</title>

    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css');?>">
    <script src="<?php echo base_url('bootstrap/js/bootstrap.min.js');?>"></script>
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/favicon.css');?>" integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
     <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker3.min.css?fbclid=IwAR3D17_Dv_8ub4h3KYnUucKv-WJmI9hxy6xd60Dx9wvZp-uOlkD-X0jGhys"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js?fbclid=IwAR2dAyQBdpHjJ6iwfeZNBmcwAENI_aqzhpDuSAL_XptbSOyXVG35PmAV0T8"></script>
     <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css?fbclid=IwAR3JktE0U22wfjcrQRZQ2iioHDiZHnSs5HIQbdJciIJxGVQFG41rxgS0p98">

  </head>

  <body>

  	<div class="clearfix"></div>
  		<div id="section-home" class="section">
  			<div class="container">
  				<div class="row">
  					<div class="col-md-5 col-left">
  						<br>
  						<br>
  						<br>
  						<br>
  						<br>
  						<h4 class="marginbot25">Booking</h4>
  							<!-- Start Form -->
                    <div class="form-book">
                        <form id="formHome">
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label>City</label>
                                    <select class="form-control" id="city" name="city" required="required">
                                        <option>Pilih Kota</option>
                                    </select>
                                    <label for="city" generated="true" class="error"></label>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>From</label>
                                    <select class="form-control from-input" id="from" name="from" required="required">
                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Pilih Lokasi</option>
                                    </select>
                                    <label for="from" generated="true" class="error"></label>
                                </div>
                                 <!-- // Rute Gak ada
                                 <div class="form-group col-md-12">
                                    <label>Rute</label>
                                    <select class="form-control rute-input" id="rute" name="rute" required="required">
                                        <option value="" disabled="disabled" selected="selected" hidden="hidden">Pilih Rute</option>
                                    </select>
                                    <label for="rute" generated="true" class="error"></label>
                                    <label class="rute-error"></label>
                                </div> 
                            -->
                                <div class="form-group col-md-8">
                                <label>Date</label>
                                        <input type="text" class="datepicker form-control" name="tgl" id="datepicker">
                                        <script>
                                        $('.datepicker').datepicker({
                                        startDate: '0d',
                                        format: "dd-mm-yyyy",
                                        todayBtn: true,
                                        todayHighlight: true,
                                        autoclose: 1,
                                        endDate: '+7d',
                                        });
                                        </script>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Jumlah</label>
                                    <input type="number" class="form-control number" placeholder="0" value="1" min="1" max="4" id="jumlah" name="jumlah" required="required" />
                                </div>
                                <div class="col-md-12 text-center margintop20">
                                    <button class="btn btn-primary">Order</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End Form -->
                </div>

                <div class="col-md-7 col-right">
                    <!-- Start Form Title -->
                    <br>
                    <br>
                    <br>
                    <br>

                    <div class="input-group form-title">
                        <label class="input-group-addon">View Schedule Board:</label>
                        <select id="select-table" class="form-control">
                        </select>
                    </div>
                    <!-- End Form Title -->
                    <!-- Start Table -->
                    <table class="table-result">
                        <thead>
                            <tr>
                                <th>Tujuan</th>
                                <th>Unit</th>
                                <th>Departure</th>
                                <th>Next Departure</th>
                                <th>Last Departure</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="5" class="text-center">
                                    <img alt="Loading" style="width:25px" src="https://booking.cititrans.co.id/static/image/loading.gif" /> Loading...
                                </td>
                                <?php ?>
                            </tr>
                        </tbody>
                    </table>
                    <!-- End Table -->
                    <p>
                        Note: Schedule can be changed due to force majour or conditions apply.
                    </p>
  					</div>
  				</div>
  			</div>
  		</div>
    <script src="<?php echo base_url('bootstrap/js/jquery-3.3.1.slim.min.js');?>" ></script>
    <script src="<?php echo base_url('bootstrap/js/popper.min.js');?>"></script>
  </body>
</html>
