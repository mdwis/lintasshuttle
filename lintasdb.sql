-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 20 Des 2018 pada 18.42
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lintasdb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
  `idBerita` int(11) NOT NULL,
  `judulBerita` int(11) NOT NULL,
  `isiBerita` int(11) NOT NULL,
  `idPetugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `penumpang`
--

CREATE TABLE IF NOT EXISTS `penumpang` (
`idPenumpang` int(11) NOT NULL,
  `idTransaksi` int(11) NOT NULL,
  `namaPenumpang` varchar(50) NOT NULL,
  `jenisKelamin` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `noHp` int(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` int(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `role` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penumpang`
--

INSERT INTO `penumpang` (`idPenumpang`, `idTransaksi`, `namaPenumpang`, `jenisKelamin`, `email`, `noHp`, `username`, `password`, `alamat`, `role`, `status`) VALUES
(1, 0, 'Dwi', 'Laki-Laki', 'admin@admin.com', 2147483647, 'dwi', 123, 'bdg', '', 0),
(2, 0, 'Dwi', 'Laki-Laki', 'admin@admin.com', 2147483647, 'asda', 123, 'bdg', '', 0),
(3, 0, 'user', 'Laki-Laki', 'usergues.only@gmail.com', 2147483647, 'user', 123, 'bdg', '', 0),
(4, 0, 'user2', 'Laki-Laki', 'rivhaldy@gmail.com', 2147483647, 'user2', 123, 'bdg', '', 0),
(5, 0, 'user3', 'Laki-Laki', 'user@gmail.com', 2147483647, 'user3', 123, 'bdg', '', 0),
(6, 47, 'user4', 'Laki-Laki', 'yudiejuandana@yahoo.co.id', 2147483647, 'user4', 123, 'bdg', '', 0),
(7, 47, 'user5', 'Laki-Laki', 'user@gmail.com', 2147483647, 'user5', 123, 'bdg', '', 0),
(8, 47, 'user6', 'Laki-Laki', 'user6@gmail.com', 2147483647, 'user6', 123, 'bdg', '', 0),
(9, 47, 'user6', 'Laki-Laki', 'user6@gmail.com', 2147483647, 'user6', 123, 'bdg', '', 0),
(10, 47, 'user7', 'Laki-Laki', 'warkop@gmail.com', 2147483647, 'user7', 123, 'bdg', '', 0),
(11, 47, 'user8', 'Laki-Laki', 'user8@gmail.com', 2147483647, 'user8', 123, 'bdg', 'user', 1),
(12, 53, 'user9', 'Laki-Laki', 'netmedia.userpaid@gmail.com', 2147483647, 'user9', 123, 'bandung', 'user', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `idPetugas` int(11) NOT NULL,
  `namaPetugas` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `role` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`idPetugas`, `namaPetugas`, `username`, `password`, `role`, `status`) VALUES
(1, 'amir', 'admin', 'admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tiket`
--

CREATE TABLE IF NOT EXISTS `tiket` (
`idTiket` int(11) NOT NULL,
  `idPetugas` int(11) NOT NULL,
  `Keberangkatan` varchar(50) NOT NULL,
  `Tujuan` varchar(50) NOT NULL,
  `Stok` int(11) NOT NULL,
  `Harga` int(11) NOT NULL,
  `Jam` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tiket`
--

INSERT INTO `tiket` (`idTiket`, `idPetugas`, `Keberangkatan`, `Tujuan`, `Stok`, `Harga`, `Jam`) VALUES
(30, 1, 'Bogor', 'Bandung', 1, 150000, '00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
`idTransaksi` int(11) NOT NULL,
  `tanggalKeberangkatan` date NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keberangkatan` varchar(50) NOT NULL,
  `tujuan` varchar(50) NOT NULL,
  `jam` time NOT NULL,
  `tempatDuduk` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`idTransaksi`, `tanggalKeberangkatan`, `jumlah`, `keberangkatan`, `tujuan`, `jam`, `tempatDuduk`) VALUES
(36, '2018-11-29', 1, 'Jakarta', 'Bandung', '00:00:00', ''),
(37, '2018-11-24', 1, 'Jakarta', 'Bandung', '15:45:00', ''),
(38, '2018-12-13', 11, 'Depok', 'Bandung', '15:45:00', ''),
(39, '2018-12-14', 11, 'Jakarta', 'Bandung', '07:38:00', ''),
(40, '2018-12-20', 1, 'Depok', 'Bandung', '15:45:00', '1'),
(41, '2018-12-23', 1, 'Depok', 'Bandung', '00:00:00', '10'),
(42, '2018-12-23', 1, 'Depok', 'Bandung', '15:45:00', '0'),
(43, '2018-12-23', 1, 'Jakarta', 'Bandung', '15:45:00', '0'),
(44, '2018-12-24', 1, 'Depok', 'Purwakarta', '06:00:00', '10'),
(45, '2018-12-23', 11, 'Depok', 'Bandung', '18:36:00', '0'),
(46, '2018-12-23', 1, 'Jakarta', 'Bandung', '10:00:00', ''),
(47, '2018-12-20', 1, 'Jakarta', 'Bandung', '00:00:00', '8'),
(48, '2018-12-23', 1, 'Jakarta', 'Bandung', '00:00:00', '7'),
(49, '2018-12-20', 11, 'Bogor', 'Purwakarta', '10:00:00', '9'),
(50, '2018-12-18', 1, 'Bogor', 'Bandung', '00:00:00', ''),
(51, '2018-12-18', 1, 'Bogor', 'Bandung', '00:00:00', ''),
(52, '2018-12-18', 1, 'Bogor', 'Bandung', '00:00:00', ''),
(53, '2018-12-19', 1, 'Bogor', 'Bandung', '00:00:00', '10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`idBerita`), ADD UNIQUE KEY `idPetugas` (`idPetugas`);

--
-- Indexes for table `penumpang`
--
ALTER TABLE `penumpang`
 ADD PRIMARY KEY (`idPenumpang`), ADD KEY `idTransaksi` (`idTransaksi`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
 ADD PRIMARY KEY (`idPetugas`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
 ADD PRIMARY KEY (`idTiket`), ADD KEY `idPetugas` (`idPetugas`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
 ADD PRIMARY KEY (`idTransaksi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `penumpang`
--
ALTER TABLE `penumpang`
MODIFY `idPenumpang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
MODIFY `idTiket` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
MODIFY `idTransaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
