-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 15 Jan 2019 pada 21.14
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lintasdb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE IF NOT EXISTS `berita` (
`idBerita` int(11) NOT NULL,
  `idPetugas` int(11) NOT NULL,
  `judulBerita` text NOT NULL,
  `isiBerita` text NOT NULL,
  `tglBerita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `penulis` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`idBerita`, `idPetugas`, `judulBerita`, `isiBerita`, `tglBerita`, `penulis`) VALUES
(1, 0, 'asdasd', 'asdas', '0000-00-00 00:00:00', ''),
(5, 1, 'sadfsd', 'asdasdasd', '0000-00-00 00:00:00', ''),
(6, 1, 'Bencana Alam', '<p>asasasas <img alt="" src="/lintasshuttle/assets/kcfinder/upload/files/1.png" style="height:141px; width:918px" /></p>', '0000-00-00 00:00:00', ''),
(7, 1, 'saas', '<p>asasasas</p>\r\n', '0000-00-00 00:00:00', ''),
(8, 1, 'Promosi', '<p>Tidak ada berita hari ini</p>\r\n', '0000-00-00 00:00:00', ''),
(9, 1, 'Resmi Diperpanjang, Ini Ruas Jalan dan Jam Pemberlakuan Peraturan Ganjil Genap', '<p>&nbsp;</p>\r\n\r\n<p><img alt="" src="/lintasshuttle/assets/kcfinder/upload/files/asd.jpg" style="height:281px; width:500px" /></p>\r\n\r\n<p>Melansir Merdeka.com, peraturan ganjil genap diberlakukan di beberapa ruas jalan Ibu Kota, mencakup Jalan Medan Merdeka Barat, MH Thamrin, Jenderal Sudirman, Jalan S Parman (mulai dari simpang Jalan Tomang Raya sampai dengan simpang Jalan KS Tubun), Jalan Jenderal MT Haryono, DI Panjaitan, Ahmad Yani, dan HR Rasuna Said.</p>\r\n\r\n<p>Sementara pada hari libur akhir pekan, Sabtu dan Minggu, serta hari libur nasional termasuk yang dikecualikan dalam peraturan gubernur ini dan akan ditetapkan dengan Keputusan Presiden.</p>\r\n\r\n<p>Peraturan ini juga tidak berlaku bagi kendaraan Pimpinan Lembaga Tinggi Negara Republik Indonesia, seperti presiden/wakil presiden, ketua MPR/DPR/DPD, ketua MA/MK/KY/BPK, kendaraan pimpinan dan pejabat negara asing serta lembaga internasional, kendaraan operasional berplat dinas milik TNI dan Polri, kendaraan pemadam kebakaran dan ambulans, kendaraan angkutan BBM, dan angkutan umum berpelat kuning.</p>\r\n', '2019-01-15 14:08:56', 'amir');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pembayaran`
--

CREATE TABLE IF NOT EXISTS `pembayaran` (
`idPembayaran` int(11) NOT NULL,
  `namaPetugas` varchar(20) NOT NULL,
  `namaBank` varchar(20) NOT NULL,
  `nomorRekening` bigint(20) NOT NULL,
  `namaPemilik` varchar(20) NOT NULL,
  `Deskripsi` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pembayaran`
--

INSERT INTO `pembayaran` (`idPembayaran`, `namaPetugas`, `namaBank`, `nomorRekening`, `namaPemilik`, `Deskripsi`) VALUES
(3, 'amir', 'BANK BRI', 317901000163254, 'Lintas Shuttle', 'Kosong'),
(4, 'amir', 'BANK CIMB NIAGA', 521888213123, 'Lintas Shuttle', 'Kosong');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penumpang`
--

CREATE TABLE IF NOT EXISTS `penumpang` (
`idPenumpang` int(11) NOT NULL,
  `idTransaksi` int(11) NOT NULL,
  `namaPenumpang` varchar(50) NOT NULL,
  `jenisKelamin` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `noHp` bigint(12) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `role` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penumpang`
--

INSERT INTO `penumpang` (`idPenumpang`, `idTransaksi`, `namaPenumpang`, `jenisKelamin`, `email`, `noHp`, `username`, `password`, `alamat`, `role`, `status`) VALUES
(1, 0, 'Dwi', 'Laki-Laki', 'admin@admin.com', 2147483647, 'dwi', '123', 'bdg', '', 0),
(2, 0, 'Dwi', 'Laki-Laki', 'admin@admin.com', 2147483647, 'asda', '123', 'bdg', '', 0),
(3, 0, 'user', 'Laki-Laki', 'usergues.only@gmail.com', 2147483647, 'user', '123', 'bdg', '', 0),
(4, 0, 'user2', 'Laki-Laki', 'rivhaldy@gmail.com', 2147483647, 'user2', '123', 'bdg', '', 0),
(5, 0, 'user3', 'Laki-Laki', 'user@gmail.com', 2147483647, 'user3', '123', 'bdg', '', 0),
(6, 47, 'user4', 'Laki-Laki', 'yudiejuandana@yahoo.co.id', 2147483647, 'user4', '123', 'bdg', '', 0),
(7, 47, 'user5', 'Laki-Laki', 'user@gmail.com', 2147483647, 'user5', '123', 'bdg', '', 0),
(8, 47, 'user6', 'Laki-Laki', 'user6@gmail.com', 2147483647, 'user6', '123', 'bdg', '', 0),
(9, 47, 'user6', 'Laki-Laki', 'user6@gmail.com', 2147483647, 'user6', '123', 'bdg', '', 0),
(10, 47, 'user7', 'Laki-Laki', 'warkop@gmail.com', 2147483647, 'user7', '123', 'bdg', '', 0),
(11, 47, 'user8', 'Laki-Laki', 'user8@gmail.com', 2147483647, 'user8', '123', 'bdg', 'user', 1),
(12, 53, 'user9', 'Laki-Laki', 'netmedia.userpaid@gmail.com', 2147483647, 'user9', '123', 'bandung', 'user', 1),
(13, 57, 'Mochamad Dwi Syafriadi', 'Laki-Laki', 'user9@gmail.com', 2147483647, 'dwisyafriadi', '0', 'Bandung', 'user', 1),
(14, 58, 'user10', 'Perempuan', 'netmedia.userpaid@gmail.com', 81214827906, 'user10', '123', 'Jakarta Kebayoran', 'user', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `idPetugas` int(11) NOT NULL,
  `namaPetugas` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `role` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`idPetugas`, `namaPetugas`, `username`, `password`, `role`, `status`) VALUES
(1, 'amir', 'admin', 'admin', 'admin', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tiket`
--

CREATE TABLE IF NOT EXISTS `tiket` (
`idTiket` int(11) NOT NULL,
  `idPetugas` int(11) NOT NULL,
  `Keberangkatan` varchar(50) NOT NULL,
  `Tujuan` varchar(50) NOT NULL,
  `Stok` int(11) NOT NULL,
  `Harga` int(11) NOT NULL,
  `Jam` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tiket`
--

INSERT INTO `tiket` (`idTiket`, `idPetugas`, `Keberangkatan`, `Tujuan`, `Stok`, `Harga`, `Jam`) VALUES
(31, 1, 'Subang', 'Bandung', 10, 120000, '10:00:00'),
(32, 1, 'Purwakarta', 'Subang', 0, 90000, '10:05:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
`idTransaksi` int(11) NOT NULL,
  `idTiket` int(11) NOT NULL,
  `tanggalKeberangkatan` date NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tempatDuduk` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`idTransaksi`, `idTiket`, `tanggalKeberangkatan`, `jumlah`, `tempatDuduk`) VALUES
(36, 0, '2018-11-29', 1, ''),
(37, 0, '2018-11-24', 1, ''),
(38, 0, '2018-12-13', 11, ''),
(39, 0, '2018-12-14', 11, ''),
(40, 0, '2018-12-20', 1, '1'),
(41, 0, '2018-12-23', 1, '10'),
(42, 0, '2018-12-23', 1, '0'),
(43, 0, '2018-12-23', 1, '0'),
(44, 0, '2018-12-24', 1, '10'),
(45, 0, '2018-12-23', 11, '0'),
(46, 0, '2018-12-23', 1, ''),
(47, 0, '2018-12-20', 1, '8'),
(48, 0, '2018-12-23', 1, '7'),
(49, 0, '2018-12-20', 11, '9'),
(50, 0, '2018-12-18', 1, ''),
(51, 0, '2018-12-18', 1, ''),
(52, 0, '2018-12-18', 1, ''),
(53, 0, '2018-12-19', 1, '10'),
(54, 32, '0000-00-00', 0, ''),
(55, 30, '0000-00-00', 0, ''),
(56, 31, '2019-01-14', 1, ''),
(57, 31, '2019-01-14', 4, '8'),
(58, 31, '2019-01-13', 1, '10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
 ADD PRIMARY KEY (`idBerita`), ADD KEY `idPetugas` (`idPetugas`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
 ADD PRIMARY KEY (`idPembayaran`);

--
-- Indexes for table `penumpang`
--
ALTER TABLE `penumpang`
 ADD PRIMARY KEY (`idPenumpang`), ADD KEY `idTransaksi` (`idTransaksi`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
 ADD PRIMARY KEY (`idPetugas`);

--
-- Indexes for table `tiket`
--
ALTER TABLE `tiket`
 ADD PRIMARY KEY (`idTiket`), ADD KEY `idPetugas` (`idPetugas`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
 ADD PRIMARY KEY (`idTransaksi`), ADD KEY `idTiket` (`idTiket`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
MODIFY `idBerita` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
MODIFY `idPembayaran` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `penumpang`
--
ALTER TABLE `penumpang`
MODIFY `idPenumpang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tiket`
--
ALTER TABLE `tiket`
MODIFY `idTiket` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
MODIFY `idTransaksi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
